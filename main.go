package main

import (
	"fmt"
	"strings"
	"sort"
	"bufio"
	"os"
	"./crawler"
	"log"
	"io/ioutil"
)


func printUsage() {
	// Help the user out by listing the functions that can be executed.
	fmt.Println("")
	fmt.Println("")
	fmt.Println("This program expects input in the form of commands and options. Each command is entered on a single line. Valid commands are:")
	fmt.Println("\tScan url")
	fmt.Println("\tSearch word1 word2 ...")
	fmt.Println("\tStatus")
	fmt.Println("\tClear")
	fmt.Println("\tList queue")
	fmt.Println("\tDetails url")
	fmt.Println("\tQuit")
	fmt.Println("")
	fmt.Println("Scan adds the URL to the list of pages to scan. The URL should be fully qualified such as https://www.google.com.")
	fmt.Println("")
	fmt.Println("Search returns the set of pages that have the most occurrences of the set of words listed.")
	fmt.Println("")
	fmt.Println("Status retrieves information about the scanning engine to see how many items are in the \"To Crawl\", \"Crawling\" and \"Crawled\" queues.")
	fmt.Println("")
	fmt.Println("Clear flushes the crawl queues and deletes all page information.")
	fmt.Println("")
	fmt.Println("List queue will displays the list of URLs in the requested queue. Valid choices for queue are \"To Crawl\", \"Crawling\" and \"Crawled\".")
	fmt.Println("")
	fmt.Println("Details url list all details about the crawled page including Title, Depth, Child URLs and list of words and the number of times each word was encountered.")
	fmt.Println("")
	fmt.Println("Quit stops the program.")
	fmt.Println("")
	fmt.Println("")
}


func cleanURL(url string) string {
	return strings.ToLower(strings.Trim(url, "\""))
}


func printSearchResults(qmgr *crawler.QueueManager, terms []string) {
	sterms := make( []string, 0, len(terms) )
	for _, v := range terms {
		v = strings.Trim(v, ",")
		v = strings.TrimSpace(v)
		if len(v) > 0 { sterms = append(sterms, strings.ToLower(v)) }
	}

	pages := qmgr.Search(sterms)

	if len(pages) == 0 {
		fmt.Printf("\nNo occurrences found for the search terms %s\n", strings.Join(sterms, ", "))
	} else {
		fmt.Printf("\nFound %d pages containing the search terms %s\n", len(pages), strings.Join(sterms, ", "))
		for _, page := range pages {
			fmt.Printf("URL: %s, Title: %s, Total Occurrences %d\n", page.Url, page.Title, page.Occurrences)
		}
	}
}


func printUrlList(qmgr *crawler.QueueManager, list string) {
	urls := qmgr.ListUrlsInQueue(list)

	if len(urls) == 0 {
		fmt.Printf("\nNo URLs found in %s queue.\n", list)
	} else {
		fmt.Printf("\nURLs in %s queue:\n", list)
		for _, value :=range urls {
			fmt.Printf("\t%s\n", value)
		}
	}

	fmt.Println()
}


func printDetails(qmgr *crawler.QueueManager, url string) {
	cleanurl := cleanURL(url)

	// Taking a shortcut here by leaking out the internal page representation. Since
	// pages are only updated when they are crawled and only crawled once, the data
	// will not change other than the page.Depth may be updated. In a production system
	// I would return prefer to return a copy of this data.
	page, status := qmgr.GetDetailsOnURL(cleanurl)
	if nil == page {
		fmt.Printf("\n%s\n", status)
	} else {
		fmt.Printf("\nDetails from crawling %s\n", page.Url)
		fmt.Printf("\tPage title %s\n", page.Title)
		fmt.Printf("\tCrawl depth %d\n", page.Depth)
		if page.Response != 0 {
			fmt.Printf("\tAn error occurred when crawling the page.\n")
		} else {
			fmt.Printf("\tLinks to descendant pages\n")

			// Display the child pages in alphabetic order
			children := make( []string, 0, len(page.Children))
			for child, _ := range page.Children { children = append( children, child ) }
			sort.Strings(children)

			for key, _ := range children {
				fmt.Printf("\t\t%s\n", key)
			}

			// Display the words in alphabetic order to make it easy to find a word (other iption would be to sor by value to see most frequent words first)
			keys := make( []string, 0, len(page.WordMap))
			for word, _ := range page.WordMap {	keys = append( keys,word ) }
			sort.Strings( keys )

			fmt.Printf("\tWord frequency\n")
			for _, word := range keys {
				fmt.Printf("\t\t%s\t%d\n", word, page.WordMap[word])
			}
		}
	}

	fmt.Println()
}


func main() {
	// Shut off logging
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)

	// Do our setup
	qmgr := crawler.GetQueueManager()

	// We can have as many concurrent goroutines crawling pages as we want.
	// Just kick them off here or anytime you want.
	for i := 0; i < 5; i++ {
		go crawler.Crawl(qmgr)
	}


	// Read lines of input
	printUsage()

	// Process user input
	scanner := bufio.NewScanner(os.Stdin)
	for {
		// Grab the input
		fmt.Printf("\n> ") 
		scanner.Scan()
		input := scanner.Text()

		// Split the search terms
		terms := strings.Split(input, " ")

		// Make sure we have something
		if len(terms) == 0 {
			printUsage()
			continue
		}

		// React to the command
		switch strings.ToLower(terms[0]) {
		case "scan":
			if len(terms) != 2 {
				fmt.Printf("\nError specifying URL to crawl. Please enter a single fully qualified URL.\n")
				continue
			}

			// Force the case to lower and strip off surrounding quotes if present.
			url := cleanURL(terms[1])
			fmt.Printf("\nAdding %s to the list of sites to crawl.\n", url)
			qmgr.AddToCrawlQueue(url, 1)
		case "search":
			if len(terms) < 2 {
				fmt.Printf("\nPlease specify at least 1 word to search for.\n")
				continue
			}

			// Get a list of pages and number of occurrences.
			printSearchResults(qmgr, terms[1:])
		case "status":
			status := qmgr.GetCrawlingStatus()
			fmt.Printf("\nCurrent crawling status:\n")
			fmt.Printf("\tPages to crawl: %d\n", status.ToCrawlCount)
			fmt.Printf("\tCurrently crawling: %d\n", status.CrawlingCount)
			fmt.Printf("\tCrawled: %d\n\n", status.CrawledCount)
		case "clear":
			fmt.Printf("\nClearing all crawl queues and data\n")
			qmgr.ClearData()
		case "list":
			if len(terms) != 2 {
				fmt.Printf("\nPlease specify which queue you want to list. Valid choices for queue are \"To Crawl\", \"Crawling\" and \"Crawled\".\n")
				continue
			}

			// Get a list of URLs in the specified queue.
			printUrlList(qmgr, terms[1])
		case "details":
			if len(terms) != 2 {
				fmt.Printf("\nPlease specify which URL you want to crawling details about.\n")
				continue
			}

			// Get a list of URLs in the specified queue.
			printDetails(qmgr, terms[1])
		case "quit":
			return
		default:
			printUsage()
		}
	}
}
