package crawler


import (
	"fmt"
	urlpackage "net/url"
	"log"
	"strings"
)

// Structure for recording data about a page.
type PageModel struct {
	Url			string					// Full URL for this page. We don't really have to store this here since it is the key into the crawledPage map, but sometimes it's very convenient to have info at our fingertips. If space is at a premium, we can drop this column.
	Title		string					// HTML page title.
	Depth		int						// What depth was this page crawled at? 1 means it was the original URL entered. 2 means child of that page...
	Response	int						// Did we get a successful query? 0 indicates success. Negative numbers are error codes. Would like to use http codes, but didn't see a quick way to do so.
	Children	map[string]	struct{} 	// We don't need to store anything beyond the URL so value is an empty struct.
	WordMap		map[string] int			// Track how many times each word occurs.
}


// Structure for search results
type SearchResults struct {
	Url			string		// Full URL for this page.
	Title		string		// HTML page title.
	Occurrences	int			// How many times did the search terms occur on the page?
}


// Structure for querying info about our current status.
type CrawlStatus struct {
	ToCrawlCount	int
	CrawlingCount	int
	CrawledCount	int
}


type empty struct {}


func NewPage(url string, depth int) *PageModel {
	page := PageModel {
		Url: url,
		Depth: depth,
		Response: -1,
		Children: make( map[string]struct{} ),
		WordMap: make( map[string]int )	}

	return &page
}


func (p *PageModel)NormalizeUrl(currentUrl *urlpackage.URL, incomingUrl string) (string, error) {
	// Make sure the new URL is not escaped
	newUrl, err := urlpackage.QueryUnescape(strings.TrimSpace(incomingUrl))
	if nil != err {
		log.Printf("Cannot un-escape url %s: %#v\n", incomingUrl, err)
		return "", err
	}

	// Case is probably not important so we should make sure URLs are consistent
	if !PRESERVE_URL_CASE { newUrl = strings.ToLower(newUrl) }

	// Parse the unescaped URL
	parsedURL, err := urlpackage.Parse(newUrl)
	if nil != err {
		log.Printf("Cannot parse url %s: %#v\n", incomingUrl, err)
		return "", err
	}

	// Build a new url
	var host string

	if len(parsedURL.Scheme) == 0 { parsedURL.Scheme = currentUrl.Scheme }
	if parsedURL.Scheme != "http" && parsedURL.Scheme != "https" {
		// log.Printf("Invalid scheme: %s, URL was %s\n", parsedURL.Scheme, incomingUrl)
		return "", fmt.Errorf("Invalid web scheme")
	}
	
	if len(parsedURL.Host) == 0 {
		host = currentUrl.Host

		// If we received an IPv6 address, format with brackets
		if strings.Contains(host, ":") { host = "[" + host + "]" }
		if len( currentUrl.Port() ) > 0 { host = host + ":" + currentUrl.Port()	}
		parsedURL.Host = host
	}

	// We need to handle relative URLs starting with "../"
	// log.Printf("ParsedURL.Path %s", parsedURL.Path)
	if strings.HasPrefix(parsedURL.Path, "../") {
		relpath := parsedURL.Path
		// Split the old URL on / boundaries
		pathparts := strings.Split( currentUrl.Path, "/" )

		// Count the number of directories to trim
		var count int = 1
		for strings.HasPrefix(relpath, "../") {
			relpath = strings.TrimPrefix(relpath, "../")
			count++
		}

		// log.Printf("Trimming %d segments off %s and appending %s", count, currentUrl.Path, relpath)
		if count >= len(pathparts) {
			log.Printf("Relative path has more parent path references than the original path has directories. Old %s: new %s\n", currentUrl.Path, parsedURL.Path)
			return "", fmt.Errorf("Invalid relative directories in URL")
		}

		pathparts = pathparts[0:len(pathparts) - count]
		parsedURL.Path = strings.Join(pathparts, "/") + "/" + relpath
	}

	// See if we need to trim the URL
	if !INCLUDE_QUERY { parsedURL.RawQuery = "" }
	if !INCLUDE_FRAGMENTS { parsedURL.Fragment = "" }

	return parsedURL.String(), nil
}


func (p *PageModel)AddChild(url string) {
	p.Children[url] = empty{}
}

