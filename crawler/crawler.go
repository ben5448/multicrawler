package crawler

import (
	"log"
	"fmt"
	"time"
	"strings"
	"regexp"
	"net/http"
	urlpackage "net/url"
	"golang.org/x/net/html"
)


func Crawl(qmgr *QueueManager) {
	for {
		// Find a job to work on
		url, depth := qmgr.GetNextUrl()
		if len(url) > 0 && depth > 0 {
			log.Printf("*** Crawling page %s at depth %d", url, depth)

			page, _ := crawlPage(url, depth)
			qmgr.FinishedCrawling(page)
		} else {
			time.Sleep(time.Second)
		}
	}
}



func crawlPage(url string, depth int) (*PageModel, error) {
	page := NewPage(url, depth)

	// Split the URL into scheme, host, port, path and filename
	parsedURL, err := urlpackage.Parse(url)
	if nil != err {
		log.Printf("Error parsing incoming URL: %s, Error: %#v\n", url, err)
		return page, err
	}

	// ([a-zA-Z0-9]{1,})
	var expstring string
	if INCLUDE_DIGITS {
		expstring = fmt.Sprintf( "(?m)([a-zA-Z0-9]{%d,})", MINIMUM_CHARACTERS )
	} else {
		expstring = fmt.Sprintf( "(?m)([a-zA-Z]{%d,})", MINIMUM_CHARACTERS )
	}

	expression, err := regexp.Compile(expstring)
	if nil != err {
		log.Printf("Error compiling regexp: %s, Error: %#v\n", expstring, err)
		return page, err
	}

	response, err := http.Get(url)
	if nil != err {
		log.Printf("Error retrieving URL: %s, Error: %#v\n", url, err)
		return page, err
	}

	bytes := response.Body
	defer bytes.Close()

	tokenizer := html.NewTokenizer(bytes)

	for {
		tokenType := tokenizer.Next()
		switch tokenType {
		case html.ErrorToken:
			page.Response = 0
			return page, nil
		case html.EndTagToken:
			// token := tokenizer.Token()
			// log.Printf("End of Token ==> %s", token.Data)
		case html.StartTagToken:
			token := tokenizer.Token()
			// log.Printf("Found Token ==> %s", token.Data)
			switch token.Data {
			case "a":
				for _, attr := range token.Attr {
					if attr.Key == "href" {
						// log.Printf("Found an anchor: %s\n", attr.Val)
						fullUrl, err := page.NormalizeUrl(parsedURL, attr.Val)
						if nil != err {
							// log.Printf("Unable to parse anchor tag: %s, error: %#v\n", attr.Val, err)
						} else {
							page.AddChild(fullUrl)
						}
					}
				}
			case "title":
				// Get the next token, which should be the title
				tokenType := tokenizer.Next()
				if html.TextToken == tokenType {
					token := tokenizer.Token()
					log.Printf("Title = %s\n", token.Data)
					page.Title = token.Data
				}
			case "style", "script":
				// Tokens where we need to eat the text chunk that follows
				// If the block is empty, we could be eating the closing token, but that's OK. We were not going to do anything with it.
				tokenizer.Next()
			}
			
		case html.TextToken:
			token := tokenizer.Token()
			// log.Printf("Text ==> %s\n", token.Data)
			// Extract the words, dropping punctuation
			for _, v := range expression.FindAllString(token.Data, -1) {
				page.WordMap[strings.ToLower(v)] ++
			}
		}
	}
}
