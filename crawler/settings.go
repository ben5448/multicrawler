package crawler


// Should the query string be included as part of the URL?
var INCLUDE_QUERY bool = false

// Should page fragments (# links relative to the page) be included as part of the URL?
var INCLUDE_FRAGMENTS bool = false

// Max depth to crawl
var MAXDEPTH int = 3

// Should we include digits as word characters?
var INCLUDE_DIGITS bool = false

// Set a miminum length on words in our map. Set to 1 to include all words.
var MINIMUM_CHARACTERS int = 3

// Should we preserve ase on URLs?
var PRESERVE_URL_CASE bool = false