package crawler

import (
	"sync"
	"log"
	"sort"
	"strings"
)


// Declare a couple of queues for various groups of pages

// Queue of pages that need to be crawled
// Each item in the map has a depth where this page is being crawled.
var toCrawlQueue map[string]int = make( map[string]int )


// Queue of pages that are currently being crawled.
// Crewling is relatively slow so we want to do that offline from requests since
// the goal is to support a website. We don't want to have the web site spin as
// we traverse and map a hierarchy of web pages.

// For each page being crawled, we want to remember the depth at which we are crawling it.
var beingCrawledQueue map[string]int = make( map[string]int )


// Map of pages that have been crawled.
var crawledPages map[string]*PageModel = make( map[string]*PageModel )


// Create a lock to make sure multiple threads don't access the queues simultaneously
var queueLock sync.Mutex


// Declare a QueueManager to group the functions into a related set of methods. Might want to do more later like allow for multiple groups of queues or types of queues
type QueueManager struct {
}


var qmgr QueueManager


func GetQueueManager() *QueueManager {
	return &qmgr
}


func (q *QueueManager)GetCrawlingStatus() *CrawlStatus {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()

	for k, v := range beingCrawledQueue {
		log.Printf("Crawling %s at depth %d\n", k, v)
	}
	return &CrawlStatus{ToCrawlCount:len(toCrawlQueue), CrawlingCount:len(beingCrawledQueue), CrawledCount:len(crawledPages)}
}


// This routine locks the queue and then defers to the internal function checkAddToCrawlQueue
func (q *QueueManager)AddToCrawlQueue(url string, depth int) {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()
	q.checkAddToCrawlQueue(url, depth)
}


func (q *QueueManager)checkAddToCrawlQueue(url string, depth int) {
	// Check how deep to crawl
	if depth > MAXDEPTH { return }

	// 3 cases to deal with here:
	//		Page could be queued to crawl. Adjust depth if we are going to crawl at a lower depth; otherwise, drop this request.
	//		Could be in progress. If we need to crawl at a lower level, add to the queue; otherwise, drop this request.
	//		Could have already been crawled:
	//			If crawled at a this depth or lower, drop this request
	//			No need to recrawl the page, but we need to re-examine descendents to see if they should be crawled.
	//		No other cases hit, add to the queue

	// Look to see if this URL is in our current queue
	currentDepth, found := toCrawlQueue[url]
	if found {
		if currentDepth <= depth { return }

		// Update the depth
		toCrawlQueue[url] = depth
		return
	}

	// See if this URL is in our crawling queue
	currentDepth, found = beingCrawledQueue[url]
	if found {
		if currentDepth <= depth { return }

		// We are crawling the page, but will need to recheck it at a deeper depth later
		toCrawlQueue[url] = depth
		return
	}

	// See if the page has already been crawled
	crawledPage, found := crawledPages[url]
	if found {
		if crawledPage.Depth <= depth { return }

		// We need to update the depth of this page to reflect the higher level request
		crawledPage.Depth = depth

		// This page has already been crawled, but the child pages may need to be crawled
		for key, _ := range crawledPage.Children {
			q.checkAddToCrawlQueue(key, depth + 1)
		}

		return
	}

	// We didn't find the page so add to the queue
	toCrawlQueue[url] = depth
	log.Printf("Adding to queue url: %s, depth: %d\n", url, depth)
	return
}


func (q *QueueManager)GetNextUrl() (string, int) {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()

	// Iterate through the toCrawlQueue looking for a good candidate page.
	// A page is not a candidate if it is currently being crawled or has been
	// crawled at a lower depth. In the case of a page that has been crawled,
	// that means the page was being crawled at a higher depth when we added this entry 
	// to the toCrawlQueue. We don't need to crawl the page, but we need to re-evaluate
	// the child nodes for crawling and then delete this URL.
	for url, depth := range toCrawlQueue {
		// Check beingCrawledQueue
		_, found := beingCrawledQueue[url]
		if found { continue }

		// Check crawledPages
		crawledPage, found := crawledPages[url]
		if found {
			// This page has already been crawled, but the child pages may need to be crawled
			for key, _ := range crawledPage.Children {
				q.checkAddToCrawlQueue(key, depth + 1)
			}

			// Remove this page from the queue because it was already crawled.
			delete(toCrawlQueue, url)
			continue
		}

		// We have a candidate to crawl
		// Move the URL from toCrawlQueue to beingCrawledQueue
		delete(toCrawlQueue, url)
		beingCrawledQueue[url] = depth
		return url, depth
	}

	// Nothing to do so return a null string and invalid depth
	return "", 0
}


func (q *QueueManager)FinishedCrawling(page *PageModel) {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()

	log.Printf("Finished with %s\n", page.Url)

	// Remove this page from the beingCrawledQueue
	delete(beingCrawledQueue, page.Url)

	// Update the page
	crawledPages[page.Url] = page

	// Add all the child pages
	for url, _ := range page.Children {
		q.checkAddToCrawlQueue(url, page.Depth + 1)
	}
}


func (q *QueueManager)ClearData() {

	for {
		// Lock and clear the queues
		queueLock.Lock()

		// Clear the toCrawlQueue
		toCrawlQueue = make( map[string]int )

		// If anything is in progress then we need to wait
		if len(beingCrawledQueue) > 0 {
			queueLock.Unlock()
			continue
		}

		// Clear the cralwedPages data
		crawledPages = make( map[string]*PageModel )
		queueLock.Unlock()
		break
	}
}


func (q *QueueManager)Search(terms []string) []SearchResults {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()

	results := make( []SearchResults, 0, 20 )

	// Walk the set of crawled pages
	for _, page := range crawledPages {
		// All search terms much be present
		allPresent := true
		var sum int
		for _, term := range terms {
			occurrences, found := page.WordMap[term]
			if found {
				sum += occurrences
			} else {
				allPresent = false
				break
			}
		}

		// If all terms were present, add to the result set
		if allPresent {
			results = append( results, SearchResults{Url: page.Url, Title: page.Title, Occurrences: sum} )
		}
	}

	sort.Slice( results, func(i, j int) bool {
		return results[i].Occurrences > results[j].Occurrences
	})

	return results
}


func (q *QueueManager)ListUrlsInQueue(list string) []string {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()

	results := make( []string, 0, 20 )

	switch strings.ToLower(list) {
	case "crawling":
		// Copy the being crawled URLs
		for value, _  := range beingCrawledQueue {
			results = append(results, value)
		}
	case "crawled":
		// Copy URLs from the crawled list
		for _, page := range crawledPages {
			results = append(results, page.Url)
		}
	default:
		// Copy the to crawl URLs
		for value, _ := range toCrawlQueue {
			results = append(results, value)
		}
	}

	sort.Strings(results)
	return results
}
		
func (q *QueueManager)GetDetailsOnURL(url string) (*PageModel, string) {
	// Lock and make sure we release the lock at the end of this routine
	queueLock.Lock()
	defer queueLock.Unlock()

	// See if the page has been crawled
	page, found := crawledPages[url]
	if found { return page, "Page has been crawled" }

	// See if the page is being crawled
	_, found = beingCrawledQueue[url]
	if found { return nil, "Page is being crawled" }

	// See if the page has been crawled
	page, found = crawledPages[url]
	if found { return nil, "Page ig going to be crawled" }

	// Page is not round
	return nil, "Page is not in the crawler"
}
