# Purpose
This is a simple, multi-threaded web crawler that shows a few techniques in Go. The crawler
package manages all the crawling and parsing. The main module just provides an mechanism to
activate the crawler package. That package could be plugged into a web server just as easily.

Hopefully someone will find this interesting and maybe useful. If you do, drop me a note.

# Usage
Kick off the program like most any Go program: "go run main.go".

This program expects input in the form of commands and options. Each command is entered on a single line. Valid commands are:

    Scan url
    Search word1 word2 ...
    Status
    Clear
    List queue
    Details url
    Quit


## Scan
Scan adds the URL to the list of pages to scan. The URL should be fully qualified such as https://www.google.com.

**Example:**

    Scan https://www.google.com

## Search
Search returns the set of pages that have the most occurrences of the set of words listed.

**Examples:**

    Search violet
    Search clear blue sky 

## Status
Status retrieves information about the scanning engine to see how many items are in the "To Crawl", "Crawling" and "Crawled" queues.

**Example:**

    Status

## Clear
Clear flushes the crawl queues and deletes all page information. Crazy as it seems, this is probably the slowest function of all the APIs because it has to wait for any pages that are being scanned to complete before clearing the crawled page buffer. All other functions queue an action or grab a lock and do their
work. Nothing else waits of anything to complete.

**Example:**

    Clear

## List queue
List queue will displays the list of URLs in the requested queue. Valid choices for queue are "To Crawl", "Crawling" and "Crawled".

**Examples:**

    List To Crawl
    List Crawling
    List Crawled

## Details
List all information about the crawled page including Title, Depth, Child URLs and list of words and the number of times each word was encountered.

**Example:**

    Details https://www.google.com

## Quit
Quit stops the program.

**Example:**

    Quit


# Assumptions
## Case for commands is overrated
The commands here are case insensitive.

## Single words are all you need
This is a terrible simplification, but it you want to deal with phrases then go for it. This simple example 
doesn't support them. It does support multiple words with a very simple algorithm; it adds all the
occurrences together. That being said, every word has to show up on a page for it to make the search results.

## Crawling is an expensive operation.
If we expect to be behind a web server, crawling
needs to be asynchronous from inserting, searching and checking current status. We
should also avoid recrawling pages if possible.

## Crawl order is not important
The order that pages are crawled is important. Pages are not guaranteed to be crawled in the order they are added.

## Recrawling at a higher depth
This code assumes we only want to crawl 3 levels deep. (If you don't like that limit, change it in crawler/settings.go.) If a page is added directly, it will be crawled at level 1. Any links (at least those linked via anchor tags) will be crawled at depth 2.

If a page has been crawled and then is queued again at a higher depth (say it was
crawled as a child page and then requested to be called as a specific URL), the page
and its children need to be re-evaluated such that more decendant pages may need to
be crawled. In this case we do not crawl the page again, but look to see if the descendant pages
have already been crawled. If so, have their descendants been crawled and so on to the maximum depth.

## Parameters and fragments are ignored for page crawling
Query parameters and page fragment identifiers are not critical to gathering child
pages. On most sites the structure is similar regardless of query parameters. If we
want to take parameters into account, we would attach them to the URL before adding
to the queue. There are options to change this behavior in crawler/settings.go

## Numeric digits are ignored
Digits are not part of words. This eliminates items like addresses and phone numbers
from crawled pages, but also drops some items lke "7th". There is an option in
crawler/settings.go to allow numbers. If you prefer to allow numbers only within
words then the regex expression in crawler/crawler.go would need to be modified.

## Really short words are not interesting to index
Words with only 1-2 characters are ignored using the regex filter. If you really want
them, the easiest way to enable them is to adjust the value in crawler/settings.go.

## Case is ignored for URLs
Case in URLs is not important. By default all URLs are converted to lower case to
avoid recrawling pages unnecessarily. There is an option to change this behavior in
crawler/settings.go.

## Case is ignored for search terms
I really don't have anything against case, but the results here are more meaningul if we ignore it.
For this problem set we do not want to differentiate words simply because they are used as the first
word in a sentence. All words are forced to lower case. Search terms are also forced to lower case
so it pretty much works out. If you want to identify proper names and acronyms, that could be an
interesting extension.

## Redirects and retries
At this point the program makes no attempt to follow redirects or retry pages that
fail, even if the failure code indicates the problem is temporary.

# TODO
There are lots of things to play with on this project like: 
- Make most of the methods and data structures in the crawler package private.  
- Create some getter/setter methods to adjust the options in the settings.go file.  
- Launch crawlers on first message queued.  
- Make the number of crawlers dynamic based on load and reasources.  
- Store the data somewhere persistent.  
- Deal with other tags.   
- Use a better logger to support logging levels. I like the MultiLogger from Revel as an example.  
- Whatever you would like to see in your crawler.  
